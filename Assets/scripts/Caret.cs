﻿using UnityEngine;
using System.Collections;

public class Caret : MonoBehaviour {
    public MeshRenderer caret;
    public float blinkTime;
    float currentBlinkTime;
	
	// Update is called once per frame
	void Update () {
        currentBlinkTime += Time.deltaTime;
        if(currentBlinkTime > blinkTime)
        {
            caret.enabled = !caret.enabled;
            currentBlinkTime = 0;
        }
	}
}
