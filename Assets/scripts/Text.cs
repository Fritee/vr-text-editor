﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Position
{
    public Position(int initX, int initY)
    {
        x = initX;
        y = initY;
    }
    public int x, y;
}

public class Text : MonoBehaviour {
    public TextMesh textMesh;
    public TextMesh caret;
    // length of the text in x dimension character wise
    public int lineLength;
    // maximum number of lines allowed
    public int amountOfLines;
    // length of text in x dimension
    public float xLength;
    // length of text in y dimension
    public float yLength;
    public string filename;
    List<string> lines = new List<string>();
    string path = null;
    Position carry = new Position(0, 0);
    Position screen = new Position(0, 0);
    Position currentCharacter = new Position(0, 0);
    void Start()
    {
        Load(filename);
    }

    public void Load(string filename)
    {
        lines.AddRange(System.IO.File.ReadAllLines(filename));
        path = filename;
        UpdateText();
    }

    void UpdateText()
    {
        textMesh.text = "";
        for(int a = screen.y; a < screen.y + amountOfLines; a++)
        {
            if (lines.Count <= a)
                break;
            for(int b = screen.x; b < screen.x + lineLength; b++)
            {
                if (lines[a].Length <= b)
                    break;
                //if (a != currentCharacter.y || b != currentCharacter.x)
                textMesh.text += lines[a][b];
                //else
                //    textMesh.text += "<b>" + lines[a][b] + "</b>";
            }
            textMesh.text += '\n';
        }
    }

    void Save()
    {
        System.IO.File.WriteAllLines(path, lines.ToArray());
    }

    public void HandleClickPoint(Vector3 point)
    {
        Vector3 localPoint = transform.InverseTransformPoint(point);
        localPoint.z = 0;
        localPoint.y += 1f;
        localPoint.x -= 0.5f;
        localPoint.y = -localPoint.y;
        localPoint.x /= xLength;
        localPoint.y /= yLength;
        localPoint.x *= lineLength;
        localPoint.y *= amountOfLines;
        currentCharacter = new Position(screen.x + (int)Mathf.Round(localPoint.x), screen.y + (int)Mathf.Round(localPoint.y));
        UpdateText();
        SetCaret();
        Debug.Log(currentCharacter.x + " " + currentCharacter.y);
    }

    public void HandleInput()
    {
        DeleteCharacter();
        HandleArrows();
        HandleCommands();
        CheckKey();
        string input = Input.inputString;
        for(int a = 0; a < input.Length; a++)
        {
            if(!char.IsLetterOrDigit(input[a]) && !(input[a] == ' '))
            {
                input = input.Remove(a, 1);
                a--;
            }
        }
        if (currentCharacter.y < 0)
            currentCharacter.y = 0;
        if (currentCharacter.x >= lines[currentCharacter.y].Length)
            currentCharacter.x = lines[currentCharacter.y].Length - 1;
        lines[currentCharacter.y] = lines[currentCharacter.y].Insert(currentCharacter.x + 1, input);
        currentCharacter.x += input.Length;
        UpdateText();
        SetCaret();
    }

    void HandleCommands()
    {
        if(Input.GetKeyDown(KeyCode.Return))
        {
            lines.Insert(currentCharacter.y + 1, "");
            lines[currentCharacter.y + 1] = lines[currentCharacter.y].Substring(currentCharacter.x + 1);
            lines[currentCharacter.y] = lines[currentCharacter.y].Remove(currentCharacter.x + 1);
            currentCharacter.y += 1;
            currentCharacter.x = lines[currentCharacter.y].Length - 1;
        }
        if(Input.GetKey(KeyCode.CapsLock) && Input.GetKeyDown(KeyCode.S))
        {
            Save();
        }
    }

    void HandleArrows()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if(currentCharacter.y != 0)
                currentCharacter.y -= 1;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (currentCharacter.y < lines.Count - 1)
                currentCharacter.y += 1;
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (currentCharacter.x != -1)
                currentCharacter.x -= 1;
            else
            {
                if (currentCharacter.y != 0)
                {
                    currentCharacter.y -= 1;
                    currentCharacter.x = lines[currentCharacter.y].Length - 1;
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (currentCharacter.x < lines[currentCharacter.y].Length - 1)
                currentCharacter.x += 1;
            else
            {
                if (currentCharacter.y < lines.Count - 1)
                {
                    currentCharacter.y += 1;
                    currentCharacter.x = 0;
                }
            }
        }
        
    }

    void SetCaret()
    {
        caret.transform.localPosition = new Vector3(
            (currentCharacter.x - screen.x) / (float)lineLength * xLength + 1.1f,
            (-currentCharacter.y + screen.y) / (float)amountOfLines * yLength + 0f,
            0);
    }

    void DeleteCharacter()
    {
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            // delete a character
            if (currentCharacter.x >= 0)
            {
                Debug.Log(currentCharacter.x + " " + currentCharacter.y);
                lines[currentCharacter.y] = lines[currentCharacter.y].Remove(currentCharacter.x, 1);
                currentCharacter = new Position(currentCharacter.x - 1, currentCharacter.y);
                SetCaret();
            }
            else
            {
                if (currentCharacter.y != 0)
                {
                    currentCharacter.x = lines[currentCharacter.y - 1].Length - 1;
                    lines[currentCharacter.y - 1] += lines[currentCharacter.y];
                    lines.RemoveAt(currentCharacter.y);
                    currentCharacter.y--;
                }
            }
        }
    }

    void CheckKey()
    {
        if (currentCharacter.x > screen.x + lineLength)
        {
            screen.x = currentCharacter.x - lineLength;
        }
        if (currentCharacter.x < screen.x)
        {
            screen.x = currentCharacter.x;
            if (screen.x < 0)
                screen.x = 0;
        }
        if(currentCharacter.y > screen.y + amountOfLines)
        {
            screen.y = currentCharacter.y;
        }
        if(currentCharacter.y < screen.y)
        {
            screen.y = currentCharacter.y;
            if (screen.y < 0)
                screen.y = 0;
        }
    }
}
