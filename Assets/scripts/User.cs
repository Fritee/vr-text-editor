﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// User input handling class
// Parts of code copied from unity questions
public class User : MonoBehaviour {
    
    public float speedH = 2.0f;
    public float speedV = 2.0f;
    public float moveAmount = 1f;

    private float yaw = 0.0f;
    private float pitch = 0.0f;

    public GameObject textWindow;
    InputState state = InputState.Free;
    Text focus = null;
    float initialDistance;

    // Use this for initialization
    void Start()
    {
        // get arguments from command line
        string[] args = System.Environment.GetCommandLineArgs();
        foreach(string arg in args)
        {
            Debug.Log(arg);
        }
    }

    void Update()
    {
        Cursor.lockState = CursorLockMode.Locked;
        CheckForOtherFiles();
        HandleInput();
        HandleMouseInput();
        HandleRotation();
        HandleMovement();
        if (Input.GetMouseButtonUp(0) && state == InputState.Holding)
        {
            state = InputState.Grabbed;
        }
        if (state == InputState.Holding)
        {
            focus.transform.LookAt(2 * focus.transform.position - transform.position);
            focus.transform.position = transform.position + transform.forward * initialDistance;
        }
    }

    void HandleInput()
    {
        if(state != InputState.Free)
        {
            focus.HandleInput();
        }
    }

    void HandleRotation()
    {
        yaw += speedH * Input.GetAxis("Mouse X");
        pitch -= speedV * Input.GetAxis("Mouse Y");

        transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
    }

    void HandleMovement()
    {
        if (state != InputState.Free)
            return;
        Vector3 offset = new Vector3(0, 0, 0);
        if (Input.GetKey(KeyCode.A))
            offset -= transform.right;
        if (Input.GetKey(KeyCode.W))
            offset += transform.forward;
        if (Input.GetKey(KeyCode.D))
            offset += transform.right;
        if (Input.GetKey(KeyCode.S))
            offset -= transform.forward;
        transform.position = transform.position + offset;
    }

    void HandleMouseInput()
    {
        if (Input.GetMouseButtonDown(0))
            OnMouseClicked();
    }

    void OnMouseClicked()
    {
        RaycastHit[] hits = Physics.RaycastAll(transform.position, transform.forward);
        if (hits.Length == 0)
            state = InputState.Free;
        for(int a = 0; a < hits.Length; a++)
        {
            if (hits[a].transform.GetComponentInParent<Text>())
            {
                Text txt = hits[a].transform.GetComponentInParent<Text>();
                txt.HandleClickPoint(hits[a].point);
                state = InputState.Holding;
                focus = txt;
                initialDistance = Vector3.Distance(transform.position, focus.transform.position);
            }
        }
    }

    void CheckForOtherFiles()
    {
        List<string> files = new List<string>();
        files.AddRange(System.IO.File.ReadAllLines("D:\\Projects\\VR\\VR Text Editor\\Assets\\files.txt"));
        if(files.Count >= 1)
        {
            GameObject tw = GameObject.Instantiate(textWindow);
            state = InputState.Holding;
            Text txt = tw.transform.GetComponent<Text>();
            txt.Load(files[0]);
            txt.transform.position = transform.position + transform.forward * 5;
            focus = txt;
            tw.transform.LookAt(tw.transform.position * 2 - transform.position);
            files.RemoveAt(0);
        }
        System.IO.File.WriteAllLines("D:\\Projects\\VR\\VR Text Editor\\Assets\\files.txt", files.ToArray());
    }
}

enum InputState
{
    Free, // Camera is free to move around using keys
    Grabbed, // all input goes to window
    Holding // moving window
}
