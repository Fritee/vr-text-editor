﻿using UnityEngine;
using System.Collections;

public class Utils : MonoBehaviour {

	public static Vector3 roundVector(Vector3 input)
    {
        return new Vector3(Mathf.Round(input.x), Mathf.Round(input.y), Mathf.Round(input.z));
    }
}
